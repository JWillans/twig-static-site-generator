<?php

namespace App\Twig;

use App\SiteGenerator\SiteGenerator;
use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Loader\FilesystemLoader;

class EnvironmentBuilder
{

    protected string $pagesDir;

    protected string $templatesDir;

    protected string $cacheDir;

    protected bool $debug;

    /**
     * @param string $pagesDir
     * @param string $templatesDir
     * @param string $cacheDir
     * @param bool $debug
     */
    public function __construct(string $pagesDir, string $templatesDir, string $cacheDir, bool $debug)
    {
        $this->pagesDir = $pagesDir;
        $this->templatesDir = $templatesDir;
        $this->cacheDir = $cacheDir;
        $this->debug = $debug;
    }

    /**
     * @return Environment
     * @throws LoaderError
     */
    public function build(): Environment
    {
        $loader = new FilesystemLoader();
        $loader->addPath($this->templatesDir);
        $loader->addPath($this->pagesDir, SiteGenerator::TWIG_PAGES_NAMESPACE);

        $options = [
            'cache' => $this->cacheDir,
            'debug' => $this->debug,
        ];

        return new Environment($loader, $options);
    }

}