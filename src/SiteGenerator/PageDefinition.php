<?php

namespace App\SiteGenerator;

class PageDefinition
{

    public function __construct(
        protected string $path,
        protected string $relPath,
        protected string $twigPath,
        protected string $outputPath,
        protected string $outputDir,
    ) {}

    /**
     * @return string
     */
    public function getPath(): string
    {
        return $this->path;
    }

    /**
     * @return string
     */
    public function getRelPath(): string
    {
        return $this->relPath;
    }

    /**
     * @return string
     */
    public function getTwigPath(): string
    {
        return $this->twigPath;
    }

    /**
     * @return string
     */
    public function getOutputPath(): string
    {
        return $this->outputPath;
    }

    /**
     * @return string
     */
    public function getOutputDir(): string
    {
        return $this->outputDir;
    }

}