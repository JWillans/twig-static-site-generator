<?php

namespace App\SiteGenerator;

use App\Filesystem\Filesystem;
use App\SiteGenerator\Exception\SiteGeneratorException;
use Symfony\Component\Finder\Finder;
use Symfony\Component\Finder\SplFileInfo;
use Twig\Environment;
use Twig\Error\LoaderError as TwigLoaderError;
use Twig\Error\RuntimeError as TwigRuntimeError;
use Twig\Error\SyntaxError as TwigSyntaxError;

class SiteGenerator
{

    const TWIG_PAGES_NAMESPACE = 'pages';

    protected string $publicDir;

    protected string $pagesDir;

    /**
     * @var array<string, mixed>
     */
    protected array $siteConfig;

    protected Environment $twig;

    protected Filesystem $filesystem;

    /**
     * @param string $publicDir
     * @param string $pagesDir
     * @param mixed[] $siteConfig
     * @param Environment $twig
     * @param Filesystem $filesystem
     */
    public function __construct(string $publicDir, string $pagesDir, array $siteConfig, Environment $twig, Filesystem $filesystem)
    {
        $this->publicDir = $publicDir;
        $this->pagesDir = $pagesDir;
        $this->siteConfig = $siteConfig;
        $this->twig = $twig;
        $this->filesystem = $filesystem;
    }

    /**
     * @return array<string, mixed>
     */
    protected function createContext(): array
    {
        return [
            'site' => $this->siteConfig,
        ];
    }

    /**
     * @return array<int, PageDefinition>
     * @throws SiteGeneratorException
     */
    protected function resolvePageDefinitions(): array
    {
        $pagesDir = $this->pagesDir;
        if(!$this->filesystem->exists($pagesDir)) throw new SiteGeneratorException(sprintf('Directory %s does not exist', $pagesDir));

        $pagesDir = realpath($pagesDir);
        if($pagesDir === false) throw new SiteGeneratorException('Unable to resolve real path of pages dir');

        $pagesDirRegex = sprintf('#^%s/#', preg_quote($pagesDir, '#'));

        $finder = Finder::create()->files()->in($this->pagesDir)->name('/.twig$/');
        /** @var SplFileInfo[] $files */
        $files = array_values(iterator_to_array($finder));

        /** @var array<int, PageDefinition|null> $definitions */
        $definitions = array_map(function(SplFileInfo $file) use ($pagesDirRegex){

            $path = realpath((string)$file);
            if($path === false) return null;

            $relPath = (string)preg_replace($pagesDirRegex, '', $path);
            $outputRelPath = preg_replace('/\.twig$/', '', $relPath);
            $outputPath = sprintf('%s/%s', $this->publicDir, $outputRelPath);
            $outputDir = dirname($outputPath);

            return new PageDefinition(
                $path,
                $relPath,
                sprintf('@%s/%s', self::TWIG_PAGES_NAMESPACE, $relPath),
                $outputPath,
                $outputDir,
            );

        }, $files);

        /** @var array<int, PageDefinition> $definitions */
        $definitions = array_filter($definitions, function(?PageDefinition $definition){
            return $definition !== null;
        });

        return array_values($definitions);
    }

    /**
     * @return array
     * @throws SiteGeneratorException
     * @param array<string, mixed> $options
     * @return array<int, PageDefinition>
     */
    public function generatePages(array $options = []): array
    {

        $options = array_merge([
            'callback' => null,
        ], $options);

        try{
            $definitions = $this->resolvePageDefinitions();
            $context = $this->createContext();

            array_walk($definitions, function(PageDefinition $definition) use ($context, $options){
                if($this->filesystem->exists($definition->getOutputPath())){
                    $this->filesystem->remove($definition->getOutputPath());
                }
                $this->filesystem->ensureDirExists($definition->getOutputDir());
                $contents = $this->twig->render($definition->getTwigPath(), $context);
                $this->filesystem->writeFile($definition->getOutputPath(), $contents);

                /** @var callable|null $callback */
                $callback = $options['callback'];
                if($callback !== null) call_user_func($callback, $definition);

            });

            return $definitions;
        }
        catch(TwigLoaderError|TwigRuntimeError|TwigSyntaxError $e){
            throw new SiteGeneratorException($e->getMessage(), 0, $e);
        }
    }

}