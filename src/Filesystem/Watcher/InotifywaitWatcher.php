<?php

namespace App\Filesystem\Watcher;

use App\Filesystem\Watcher\Exception\WatcherDependencyException;
use Symfony\Component\Process\Process;

class InotifywaitWatcher extends AbstractBinaryWatcher
{

    protected static function checkDependencies(): void
    {
        $process = new Process(['which', 'inotifywait']);
        $process->run();
        if(!$process->isSuccessful()){
            throw new WatcherDependencyException('inotify-tools not installed');
        }
    }

    protected function parseProcessDataFilePath(string $data): ?string
    {
        $path = realpath(trim($data));
        if($path === false) return null;
        return $path;
    }

    protected function createProcess(): Process
    {
        $command = [
            'inotifywait',
            '--monitor',
            '--recursive',
            '--quiet',              // Suppress all output other than formatted event data
            '--event', 'modify',    // Only listen for changes
            '--format', '%w%f',     // Output full file paths
        ];

        $command = array_merge($command, $this->dirs);

        $process = new Process($command, null, null, null, 0);
        $process->setPty(false);
        $process->setTty(false);
        $process->start();

        return $process;
    }




}