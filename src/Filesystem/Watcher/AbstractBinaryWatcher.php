<?php

namespace App\Filesystem\Watcher;

use Symfony\Component\Process\Process;

abstract class AbstractBinaryWatcher extends AbstractWatcher
{

    const REGEX_IGNORED_PATHS = '/~$/';

    abstract protected function createProcess(): Process;

    /**
     * Takes the output from a watcher process (like inotifywait) and extracts the path for the file which has been modified
     *
     * @param string $data
     * @return string|null
     */
    abstract protected function parseProcessDataFilePath(string $data): ?string;

    protected static function getRootDir(): string
    {
        return __DIR__ . '/../../..';
    }

    protected function doWatch(): void
    {
        $process = $this->createProcess();

        $lastModified = time(); // Used to prevent triggers for writes to the same file in quick succession
        $lastPath = null;

        foreach($process as $data){

            $path = $this->parseProcessDataFilePath($data);
            if($path === null || preg_match(self::REGEX_IGNORED_PATHS, $path)) continue;

            $modified = time();

            // Prevent multiple triggers on the same file within 1 second of each other
            if($path !== $lastPath || $modified !== $lastModified){
                $this->triggerListeners($path);
            }

            $lastModified = $modified;
            $lastPath = $path;
        }

    }

}