<?php

namespace App\Filesystem\Watcher;

use App\Filesystem\Watcher\Exception\WatcherDependencyException;

class WatcherDiscovery
{

    /**
     * @return AbstractWatcher
     * @throws WatcherDependencyException
     */
    public static function createWatcher(): AbstractWatcher
    {
        if(InotifywaitWatcher::hasMetDependencies()) return new InotifywaitWatcher();
        if(ChokidarWatcher::hasMetDependencies()) return new ChokidarWatcher();

        throw new WatcherDependencyException('Run `apt install inotify-tools` or `npm install chokidar` to use the file watcher');
    }

}