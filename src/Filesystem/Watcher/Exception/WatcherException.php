<?php

namespace App\Filesystem\Watcher\Exception;

use App\Filesystem\Exception\FilesystemException;

class WatcherException extends FilesystemException
{

}