<?php

namespace App\Filesystem\Watcher;

use App\Filesystem\Exception\FilesystemException;
use App\Filesystem\Watcher\Exception\WatcherDependencyException;

abstract class AbstractWatcher
{

    /**
     * @var callable[]
     */
    protected array $listeners = [];

    protected bool $started = false;

    /**
     * @var string[]
     */
    protected array $dirs = [];

    public function addListener(callable $listener): void
    {
        $this->listeners[] = $listener;
    }

    /**
     * @param string $path Path to file which has been modified
     * @return void
     */
    protected function triggerListeners(string $path): void
    {
        foreach($this->listeners as $listener){
            call_user_func($listener, $path);
        }
    }

    /**
     * @return void
     * @throws FilesystemException
     */
    protected function assertNotStarted(): void
    {
        if(!$this->started) return;
        throw new FilesystemException('Watcher cannot be modified once it has been started');
    }

    /**
     * @param string $dir
     * @return void
     * @throws FilesystemException
     */
    public function addDir(string $dir): void
    {
        $this->assertNotStarted();
        $this->dirs[] = $dir;
    }

    /**
     * @return void
     * @throws FilesystemException
     */
    public function watch(): void
    {
        $this->assertNotStarted();
        $this->started = true;
        $this->doWatch();
    }

    /**
     * @return void
     * @throws WatcherDependencyException
     */
    abstract protected static function checkDependencies(): void;

    /**
     * @return bool
     */
    public static function hasMetDependencies(): bool
    {
        try{
            static::checkDependencies();
            return true;
        }
        catch(WatcherDependencyException $e){
            return false;
        }
    }

    abstract protected function doWatch(): void;

}