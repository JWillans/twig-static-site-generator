<?php

namespace App\Filesystem\Watcher;

use App\Filesystem\Watcher\Exception\WatcherDependencyException;
use Symfony\Component\Process\Process;

class ChokidarWatcher extends AbstractBinaryWatcher
{

    protected function generateJavascript(): string
    {
        return sprintf(
            "const chokidar = require('chokidar'); chokidar.watch(%s).on('change', (path) => { console.log(path); });",
            json_encode($this->dirs)
        );
    }

    protected function createProcess(): Process
    {
        $command = [
            'node',
            '-e', $this->generateJavascript(),
        ];

        $process = new Process($command, $this->getRootDir(), null, null, 0);
        $process->setPty(false);
        $process->setTty(false);
        $process->start();

        return $process;
    }

    protected function parseProcessDataFilePath(string $data): ?string
    {
        $path = realpath(trim($data));
        if($path === false) return null;
        return $path;
    }

    protected static function getPackageDir(): string
    {
        return static::getRootDir() . '/node_modules/chokidar';
    }

    protected static function checkDependencies(): void
    {
        $packageDir = self::getPackageDir();
        if(!file_exists($packageDir)) throw new WatcherDependencyException('NodeJS package chokidar is not installed');
    }

}