<?php

namespace App\Filesystem;

use Symfony\Component\Filesystem\Exception\IOException;
use Symfony\Component\Filesystem\Filesystem as SymfonyFilesystem;

class Filesystem extends SymfonyFilesystem
{

    /**
     * @param string $dir
     * @param int $mode
     * @return void
     * @throws IOException
     */
    public function ensureDirExists(string $dir, int $mode = 0777): void
    {
        if($this->exists($dir)) return;
        $this->mkdir($dir, $mode);

        /**
         * PHPStan has issues with the exists() method, so we have to explicitly declare it as bool here
         * @var bool $exists
         */
        $exists = $this->exists($dir);

        if(!$exists){
            throw new IOException(sprintf('Dir %s could not be created', $dir));
        }
    }

    /**
     * @param string $path
     * @return string
     * @throws IOException
     */
    public function readFile(string $path): string
    {
        $contents = file_get_contents($path);
        if($contents === false) throw new IOException(sprintf('Could not read file %s', $path));
        return $contents;
    }

    /**
     * @param string $path
     * @param string $contents
     * @return void
     * @throws IOException
     */
    public function writeFile(string $path, string $contents): void
    {
        $dir = dirname($path);
        $this->ensureDirExists($dir);
        if(file_put_contents($path, $contents) === false){
            throw new IOException(sprintf('Could not write to file %s', $path));
        }
    }

}