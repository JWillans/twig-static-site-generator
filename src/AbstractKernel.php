<?php

namespace App;

use App\Filesystem\Filesystem;
use App\SiteGenerator\SiteGenerator;
use App\Twig\EnvironmentBuilder as TwigEnvironmentBuilder;
use Psr\Container\ContainerInterface;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\Definition;
use Symfony\Component\DependencyInjection\Parameter;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBag;
use Symfony\Component\DependencyInjection\Reference;
use Symfony\Component\Dotenv\Dotenv;
use Symfony\Component\Yaml\Yaml;
use Twig\Environment as TwigEnvironment;

abstract class AbstractKernel
{

    private ?Filesystem $filesystem = null;

    private ?ContainerInterface $container = null;

    private bool $dotEnvLoaded = false;

    protected function getEnvironment(): string
    {
        $this->loadDotEnv();
        return (string)$_SERVER['APP_ENV'];
    }

    protected function getDebug(): bool
    {
        return $this->getEnvironment() === 'dev';
    }

    protected function getRootDir(): string
    {
        return __DIR__ . '/..';
    }

    protected function getPublicDir(): string
    {
        return __DIR__ . '/../public';
    }

    protected function getSiteConfigYamlPath(): string
    {
        return __DIR__ . '/../config/site.yaml';
    }

    protected function getTemplatesDir(): string
    {
        return __DIR__ . '/../templates';
    }

    protected function getPagesDir(): string
    {
        return __DIR__ . '/../pages';
    }

    protected function getCacheDir(): string
    {
        return __DIR__ . '/../var/cache';
    }

    protected function loadDotEnv(): void
    {
        if($this->dotEnvLoaded) return;

        $dotenv = new Dotenv();
        $rootDir = $this->getRootDir();

        $dotenv->load(
            $rootDir . '/.env',
            $rootDir . '/.env.local'
        );

        $this->dotEnvLoaded = true;
    }

    protected function getFilesystem(): Filesystem
    {
        if($this->filesystem !== null) return $this->filesystem;
        $this->filesystem = new Filesystem();
        return $this->filesystem;
    }

    public function clearCache(): void
    {
        $cacheDir = $this->getCacheDir();
        $filesystem = $this->getFilesystem();
        if(!$filesystem->exists($cacheDir)) return;
        $filesystem->remove($cacheDir);
    }

    /**
     * @return array<string, mixed>
     */
    private function loadSiteConfig(): array
    {
        /** @var array<string, mixed> $config */
        $config = Yaml::parseFile($this->getSiteConfigYamlPath());
        return $config;
    }

    abstract protected function configureContainer(ContainerBuilder $container): void;

    /**
     * @return array<string, mixed>
     */
    public function getParameters(): array
    {
        return [
            'environment' => $this->getEnvironment(),
            'debug' => $this->getDebug(),
            'site_config' => $this->loadSiteConfig(),
            'public_dir' => $this->getPublicDir(),
            'cache_dir' => $this->getCacheDir(),
            'twig_cache_dir' => sprintf('%s/twig', $this->getCacheDir()),
            'pages_dir' => $this->getPagesDir(),
            'templates_dir' => $this->getTemplatesDir(),
        ];
    }

    protected function createContainer(): ContainerInterface
    {
        $container = new ContainerBuilder(new ParameterBag($this->getParameters()));
        $container->setDefinition(Filesystem::class, new Definition(Filesystem::class));
        $this->configureContainerForTwig($container);
        $this->configureContainerForSiteGenerator($container);
        $this->configureContainer($container);
        return $container;
    }

    protected function configureContainerForSiteGenerator(ContainerBuilder $container): void
    {
        $container->setDefinition(SiteGenerator::class, new Definition(SiteGenerator::class, [
            new Parameter('public_dir'),
            new Parameter('pages_dir'),
            new Parameter('site_config'),
            new Reference(TwigEnvironment::class),
            new Reference(Filesystem::class),
        ]));
    }

    private function configureContainerForTwig(ContainerBuilder $container): void
    {
        $container->setDefinition(TwigEnvironmentBuilder::class, new Definition(
            TwigEnvironmentBuilder::class,
            [
                new Parameter('pages_dir'),
                new Parameter('templates_dir'),
                new Parameter('twig_cache_dir'),
                new Parameter('debug')
            ]
        ));

        $container->setDefinition(
            TwigEnvironment::class,
            (new Definition(TwigEnvironment::class))
                ->setFactory([new Reference(TwigEnvironmentBuilder::class), 'build'])
        );
    }

    public function getContainer(): ContainerInterface
    {
        if($this->container !== null) return $this->container;
        $this->container = $this->createContainer();
        return $this->container;
    }

}