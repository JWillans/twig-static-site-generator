<?php

namespace App;

use Symfony\Component\DependencyInjection\ContainerBuilder;

class Kernel extends AbstractKernel
{

    protected function configureContainer(ContainerBuilder $container): void
    {
        // Customise stuff here
    }

}