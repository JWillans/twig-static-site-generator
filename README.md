# Static Site Generator

## Usage

* Run `composer install`
* Update `config/site.yaml` with your global template variables
* Create public facing page templates in `/pages`
* Run `bin/build` to generate HTML
* Run `bin/serve` to start dev server
* Run `bin/watch` to rebuild on file changes

Site builds are written to `/public`

### Using `bin/watch`

In order to use `bin/watch` you'll need a library capable of running inotify.

Just run ***one*** of the following commands to install the necessary dependencies.

* `sudo apt install inotify-tools`
* `npm install chokidar`

## @todo

* ~~Watch `/config`, `/pages` and `/templates` dir for page builds~~
* Better CLI tools
* Explain where template global variables come from
* Add `.htaccess` file to allow pages to be loaded without `.html` file extensions